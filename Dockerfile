FROM nginx:1.14.0

ADD ./kube-bosnd /kube-bosnd

CMD ["/kube-bosnd", "run", "-c", "/kube-bosnd-data/kube-bosnd.yaml"]

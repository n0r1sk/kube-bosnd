# Kube-Bosnd

Kube-Bosnd, the Kubernetes based boatswain daemon. Dynamic, Kubernetes information based configuration file template daemon for all kinds of services (Apache httpd, Nginx, HAProxy, ...) made real. Kube-Bosnd means boatswain daemon (the bosun). The word itself comes from ```bos'n``` which is a pronunciation used by people with sea legs. Kube-Bosnd is a derived fork of our Docker Swarm based [Bosnd](https://gitlab.com/n0r1sk/bosnd).

**Kube-Bosnd can be used to build any kind of Ingress controller!**

## What it does

```Kube-Bosnd``` takes a configuration file as argument and based on that configuration file, it uses the `kube-api` to retrieve information from the Kubernetes cluster. Therefore the Kube-Bosnd daemon connects to the Kubernetes API. Based on the given Kubernetes namespace, it will retrieve the IP address and the Label information of all Pods that have a Label called `kbds` (kube bosnd daemon service). All Pods with this label are sorted by the labels value and inserted into a Golang structure which can be accessed due to the Golang template syntax.

Here is an example with a Kubernetes manifest that defines a Kubernetes `Namespace` and two Kubernetes `Deployments`:

~~~yaml
apiVersion: v1
kind: Namespace
metadata:
  name: nginx-kube-bosnd-test
---
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata:
  name: nginx-deployment-a
  namespace: nginx-kube-bosnd-test
spec:
  selector:
    matchLabels:
      kbds: nginxa
  replicas: 2
  template:
    metadata:
      labels:
        kbds: nginxa
    spec:
      containers:
      - name: nginx
        image: n0r1skcom/echo
        ports:
        - containerPort: 80
---
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata:
  name: nginx-deployment-b
  namespace: nginx-kube-bosnd-test
spec:
  selector:
    matchLabels:
      kbds: nginxb
  replicas: 2
  template:
    metadata:
      labels:
        kbds: nginxb
    spec:
      containers:
      - name: nginx
        image: n0r1skcom/echo
        ports:
        - containerPort: 80
~~~

The Kube-Bosnd will now inspect all the Pods in the namespace `nginx-kube-bosnd-test`, look at their `kbds` label, take the value from the `kbds` label and build up a string map (called Services) where the key is the value of `kbds`. Beneath this key, it will sort the Pods of this `kbds` label based on their hostname and add the information about the IP address and all the other label which are attached to the Pods (see [types.go](types.go)). The result will be a structure like this:

~~~
Services
  - nginxa
    - nginxa-deploment-containera
      - 192.128.1.1
      - Labels  map[string]string
    - nginxa-deploment-containerb
      - 192.128.1.1
      - Labels  map[string]string
  - nginxb
    - nginxb-deploment-containera
      - 192.128.11.1
      - Labels  map[string]string
    - nginxb-deploment-containerb
      - 192.128.11.1
      - Labels  map[string]string
~~~

This `Services` structure can then be used for the Golang template usage.

Furthermore, if configured, Kube-Bosnd will register its own IP address (the Pod IP address of the Kube-Bosnd Pod) as a DNS A-record into the CoreDNS by writing the information to ETCD.

## Features

- Multiple configuration templates
- Usable with any daemon binary
- Automatically reload the daemon on every change (if supported by the daemon) 
- Reconfigurable during runtime
- Periodically checks for changes in the Kubernetes namespace
- Recognizes new services within the namespace which can be automatically used within the template (loop)
- Kube-Bosnd can register itself with the CoreDNS by writing to ETCD
- Optional switchable debug mode including Golang pprof interface
- sprig Golang template parsing is supported (https://github.com/Masterminds/sprig)

## Why don't use xyz...

Kube-Bosnd is not meant to replace an already existing software. Like Traefik? Use it! Like Envoy? Use it!

## Is confd similar?

As we wrote Kube-Bosnd we soon recognized, that we are writing something like [confd](https://github.com/kelseyhightower/confd). Thats true. But with a different approach. Confd will propagate the affected daemon with the new configuration and then reload/restart it. Kube-Bosnd in the opposite will be the number one process inside the container. It is responsible for the invoked daemon. If Kube-Bosnd dies, the container dies and it will be restarted. Yes, there is always a discussion if there should be only one process inside a container but this is the decision we made (more than one process) for us because we need a general purpose tool. Next, instead of enforcing the possible combination of labels (like Traefik does;no critics here), any Label can be used as data plane because Kube-Bosnd uses the full capability of the Golang Template language. If there is the need for the port information of the service, add a label and then use it in the template. If the context of the web-application is needed for configuration, add a label.

## Configuration example

Here is a full configuration example on how to use Kube-Bosnd as Ingress controller for the Kubernetes dashboard.

~~~yaml
debug: true
checkintervall: 10
cmd:
  start: ["nginx", "-g", "daemon off;"] 
  reload: ["nginx", "-s", "reload"] 
  processname: nginx
templates:
  nginx:
    src: /kube-bosnd/config/nginx.template
    dst: /etc/nginx/nginx.conf
kubernetes:
  kubeconfig: /kube-bosnd/config/kube-config # Optional, if not set, running inside Kubernetes is expected
  labelselector: k8s-app=kubernetes-dashboard # Optional, this is a prefilter. You have to label your Pods with "kbds="
  namespace: kube-system
coredns:
  arecord: dashboard.mydomain.com
  path: /dns-internal
  etcd: ["https://<ipaddress1>:2379", "https://<ipaddress1>:2379", "https://<ipaddress1>:2379"] 
control: # Optional, trigger service reload from outside
  port: 3333
  key: <MYSUPERSECRETAPIKEY>
cron: # Optional, trigger service reload periodically (https://godoc.org/github.com/robfig/cron)
  crontab: 0 30 * * * *
~~~

## Template files

The template files are working with the [Golang template language](https://golang.org/pkg/text/template/).

## Control Interface (optional)

The control interface can be used to force a service reload from the outside via http. This is useful, if a services is relying on SSL certificates which are renewed by a LetsEncrypt container for example. If this container renews the certificates it is needed to reload the service which is controlled by the Kube-Bosnd too.

Configuration:

```
control:
  port: 3333
  key: <MYSUPERSECRETAPIKEY>
```

Usage:

Trigger the following url after enabling to trigger the reload: 
```
$ curl http://127.0.0.1:3333/reload/mykey
$ Reloaded!
```
